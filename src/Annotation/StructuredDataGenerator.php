<?php

namespace Drupal\structured_data_generator\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Plugin namespace: Plugin\StructuredDataGenerator.
 *
 * @Annotation
 */
class StructuredDataGenerator extends Plugin {

  /**
   * The plugin Id.
   *
   * @var string
   */
  public $id;

}
