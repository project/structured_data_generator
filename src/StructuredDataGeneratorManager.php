<?php

namespace Drupal\structured_data_generator;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Manages StructuredDataPlugins.
 */
class StructuredDataGeneratorManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/StructuredDataGenerator',
      $namespaces,
      $module_handler,
      'Drupal\structured_data_generator\StructuredDataGeneratorInterface',
      'Drupal\structured_data_generator\Annotation\StructuredDataGenerator'
    );

    $this->alterInfo('structured_data_generator_info');
    $this->setCacheBackend($cache_backend, 'structured_data_generator_plugins');
  }

}
