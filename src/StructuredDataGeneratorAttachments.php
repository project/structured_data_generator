<?php

namespace Drupal\structured_data_generator;

use Spatie\SchemaOrg\BaseType;

/**
 * Class StructuredDataGeneratorAttachments.
 *
 * @package Drupal\structured_data_generator
 */
class StructuredDataGeneratorAttachments {

  /**
   * Adds attachments.
   *
   * Determine which attachments need to be attached
   * and attach them.
   *
   * @param array $attachments
   *   The attachments that are modified.
   */
  public function addAttachments(array &$attachments) {
    foreach ($this->getProviders() as $provider) {
      $settings = \Drupal::config('structured_data_generator.settings');
      if ($settings->get('sdg_plugin_settings.disabled_plugins') && $settings->get('sdg_plugin_settings.disabled_plugins')[$provider->getPluginId()] !== NULL) {
        continue;
      }
      $this->addToAttachments($attachments, $provider->getId(), $provider->getStucturedData());
    }
  }

  /**
   * Get Providers.
   *
   * @return \Drupal\structured_data_generator\StructuredDataGeneratorInterface[]
   *   Array of providers;
   */
  private function getProviders() {
    $type = \Drupal::service('plugin.manager.structured_data_generator');
    $plugin_definitions = $type->getDefinitions();
    $plugins = [];
    foreach ($plugin_definitions as $pluginDefinition) {
      $plugin = $type->createInstance($pluginDefinition['id'], []);
      $plugins[] = $plugin;
    }

    return $plugins;
  }

  /**
   * Add structured data to attachments.
   *
   * @param array $attachments
   *   The attachments to add data in.
   * @param string $id
   *   Id of the plugin adding data.
   * @param \Spatie\SchemaOrg\BaseType|null $structuredData
   *   StructuredData or null if none were returned.
   */
  private function addToAttachments(array &$attachments, $id, ?BaseType $structuredData) {
    if (!isset($structuredData)) {
      return;
    }

    $attachments['#attached']['html_head'][] = [
      // The data.
      [
        '#type'       => 'html_tag',
        // The HTML tag to add, in this case a  tag.
        '#tag'        => 'script',
        // The value of the HTML tag, here we want to end up with
        // alert("Hello world!");.
        '#value'      => $this->toJson($structuredData),
        // Set attributes like src to load a file.
        '#attributes' => ['type' => 'application/ld+json'],

      ],
      // A key, to recognize this HTML element when altering.
      $id,
    ];
  }

  /**
   * Get StructuredData as Json.
   *
   * @return string
   *   Json encoded string.
   */
  protected function toJson($structuredData) {
    return json_encode($structuredData->toArray(), JSON_UNESCAPED_UNICODE);
  }

}
