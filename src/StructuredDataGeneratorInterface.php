<?php

namespace Drupal\structured_data_generator;

use Spatie\SchemaOrg\BaseType;

/**
 * Interface for StructuredData plugins.
 */
interface StructuredDataGeneratorInterface {

  /**
   * Add attachment to attachments.
   *
   * @return \Spatie\SchemaOrg\BaseType|null
   *   Schema to be returned
   */
  public function getStucturedData():?BaseType;

  /**
   * Identifier for the plugin.
   *
   * @return string
   *   Identifier to be returned
   */
  public function getId():string;

}
