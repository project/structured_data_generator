<?php

namespace Drupal\structured_data_generator\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StructuredDataGeneratorSettingsForm extends ConfigFormBase {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;


  /**
   * Constructs a structured_data_generatorSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    parent::__construct($config_factory);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'structured_data_generator_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['structured_data_generator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
//    $form['#attached']['library'][] = 'structured_data_generator/listener';

    $config = $this->config('structured_data_generator.settings');

    // Main description.
    $form['sdg_description'] = [
      '#markup' => $this->t('This module generates structured data based on plugins.'),
    ];

    $form['sdg_plugin_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Plugin settings'),
      '#collapsible' => TRUE,
      '#tree' => TRUE,
      '#open' => TRUE,
      '#description' => $this->t('Options for the different plugins.'),
    ];

    $form['sdg_plugin_settings']['disabled_plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Disabled plugins'),
      '#description' => $this->t('All plugins selected will not generate their data.'),
      '#multiple' => TRUE,
      '#default_value' => $config->get('sdg_plugin_settings.disabled_plugins') ?? [],
      '#options' => $this->getPluginIds()
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Set list of disabled plugins
    $disabled_plugins = $form_state->getUserInput()['sdg_plugin_settings']['disabled_plugins'];
    $plugins = array_keys($this->getPluginIds());
    if ($disabled_plugins && count(array_intersect($plugins, array_keys($disabled_plugins))) !== count($plugins)) {
      $args = [
        '@list' => implode(',', array_intersect($plugins, array_keys($disabled_plugins))),
      ];
      $form_state->setErrorByName('sdg_plugin_settings][disabled_plugins][list', $this->t('Non existent plugin selected: @list', $args));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('structured_data_generator.settings');
    $input = $form_state->getUserInput();

    $disabled_plugins = $input['sdg_plugin_settings']['disabled_plugins'];
    $config->set('sdg_plugin_settings.disabled_plugins', $disabled_plugins);

    $config->save();

    parent::submitForm($form, $form_state);
  }

  private function getPluginIds(): array {
    /** @var \Drupal\structured_data_generator\StructuredDataGeneratorManager  $plugin_manager */
    $plugin_manager = \Drupal::service('plugin.manager.structured_data_generator');
    $definitions =  $plugin_manager->getDefinitions();
    $ordered_definitions = [];
    foreach ($definitions as $definition) {
//      $ordered_definitions[$definition['provider']] = $definition['provider'];
      $ordered_definitions[$definition['id']] = $definition['id'];
    }

    ksort($ordered_definitions);

    return $ordered_definitions;
  }

}
