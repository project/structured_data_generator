<?php

namespace Drupal\structured_data_generator\Plugin\StructuredDataGenerator;

use Drupal\Core\Plugin\PluginBase;
use Drupal\structured_data_generator\StructuredDataGeneratorInterface;
use Spatie\SchemaOrg\BaseType;
use Spatie\SchemaOrg\Schema;

/**
 * Annotation for plugin.
 *
 * @\Drupal\structured_data_generator\Annotation\StructuredDataGenerator(
 *  id = "breadcrumb_sdg",
 * )
 */
class BreadcrumbPlugin extends PluginBase implements StructuredDataGeneratorInterface {

  private $breadcrumbManager;

  private $routeMatch;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->breadcrumbManager = \Drupal::service('breadcrumb');
    $this->routeMatch        = \Drupal::service('current_route_match');
  }

  /**
   * Add attachment to attachments.
   *
   * @return \Spatie\SchemaOrg\BaseType|null
   *   Schema to be returned
   */
  public function getStucturedData(): ?BaseType {
    /** @var \Drupal\Core\Breadcrumb\Breadcrumb $breadcrumb */
    $breadcrumb = $this->breadcrumbManager->build($this->routeMatch);

    $breadcrumbList = Schema::breadcrumbList();

    $itemListElements = [];
    foreach ($breadcrumb->getLinks() as $index => $link) {
      $itemListElement = Schema::listItem();
      /** @var \Drupal\Core\Link $link */
      $itemListElement->name($link->getText());
      $itemListElement->position($index + 1);
      $linkItem = Schema::thing();

      $linkItem->setProperty('@id', $link->getUrl()
        ->setAbsolute(TRUE)
        ->toString());
      $itemListElement->item($linkItem);
      $itemListElements[] = $itemListElement;
    }
    if (empty($itemListElements)) {
      return NULL;
    }

    $breadcrumbList->itemListElement($itemListElements);

    return $breadcrumbList;

  }

  /**
   * Identifier for the plugin.
   *
   * @return string
   *   Identifier to be returned
   */
  public function getId(): string {
    return 'breadcrumb_sdg';
  }

}
